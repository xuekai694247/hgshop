<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/4
  Time: 1:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<form action="${pageContext.request.contextPath}/user/pay" method="post">
    <input type="hidden" name="id" value="${order.id}">
    <div class="container-fluid">
        <table>
            <tr>
                <td>排序</td>
                <td>商品名称</td>
                <td>商品价格</td>
                <td>购买数量</td>
                <td>价格</td>
            </tr>
            <c:forEach items="${order.orderItems}" var="each" varStatus="status">
                <tr>
                    <td>${status.index+1}</td>
                    <td>${each.goods.name}</td>
                    <td>${each.goods.price}</td>
                    <td>${each.buyNum}</td>
                    <td>${each.goods.price*each.buyNum}</td>
                </tr>
            </c:forEach>
            <tr>
                <td align="right">
                    总价格:${order.price}<input type="submit" value="去支付">
                </td>
            </tr>
        </table>
    </div>
</form>
</body>
</html>
