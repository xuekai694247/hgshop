<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/3
  Time: 12:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<!--引入导航条-->
<%--<jsp:include page="navigation.jsp"/>--%>
<form action="${pageContext.request.contextPath}/good/list" method="post">
    <input type="hidden" name="pageNum" value="${pageInfo.pageNum}">
</form>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">昊哥商城首页</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                    个人信息
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/user/home">个人主页</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/user/toLogin">登录</a>
                    <a class="dropdown-item" href="#">登出</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <c:forEach items="${pageInfo.list}" var="good">
        <div class="col-3">
            <div class="border" style="padding: 10px">
                <img src="http://localhost:91/img/${good.picture}" width="200px" height="150px"/>
                <span>
                    <br/>${good.name}<br/>
                        <font style="color: red">${good.price}</font><br/>
                    ${good.title}<br/>
                    <a href="javascript:addCart(${good.id})">加入购物车</a>
                </span>
            </div>
        </div>
        </c:forEach>
    </div>
    <button onclick="goPage(1)" class="btn btn-primary">首页</button>
    <button onclick="goPage(${pageInfo.pageNum==1?1:pageInfo.pageNum-1})" class="btn btn-primary">上一页</button>
    <button onclick="goPage(${pageInfo.pageNum==pageInfo.pages?pageInfo.pages:pageInfo.pageNum+1})" class="btn btn-primary">下一页</button>
    <button onclick="goPage(${pageInfo.pages})" class="btn btn-primary">尾页</button>

    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-toolbar">
    关于我们
</nav>
<script type="text/javascript">
    $(function(){

    })
    function goPage(pageNum){
        $("[name='pageNum']").val(pageNum);
        $("form").submit();
    }
    function addCart(id){
        $.post("${pageContext.request.contextPath}/good/add",{id:id},function(result){
            if(result.code==200){
                alert(result.data);
            }else{
                alert(result.data);
            }
        },"json")
    }
</script>
</body>
</html>
