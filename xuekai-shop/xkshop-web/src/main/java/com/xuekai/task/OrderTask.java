package com.xuekai.task;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.xuekai.pojo.Order;
import com.xuekai.service.OrderService;
import com.xuekai.utils.AlibabapayGateWayUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Component
@EnableScheduling
@Lazy(false)
public class OrderTask {

    @DubboReference
    private OrderService orderService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Resource
    private KafkaTemplate kafkaTemplate;

    @Scheduled(cron = "0 * * * * ?")
    public void test() throws AlipayApiException {
        //查询5分钟之前
        List<Order> list=orderService.listUnpay(5);
        for (Order order : list) {
            HashMap<String,String> map = new HashMap<>();
            map.put("out_trade_no",order.getId());
            AlipayTradeQueryResponse alipayTradeQueryResponse = AlibabapayGateWayUtils.queryOrder(map);
            if("TRADE_SUCCESS".equals(alipayTradeQueryResponse.getTradeStatus())){
                orderService.updateStatus(order.getId(),1);
            }
        }
    }

  /*  @Scheduled(cron = "0 * * * * ?")
    public void test1(){
        List<Order> order = redisTemplate.opsForHash().values("order");
        if(order==null){
            return;
        }
        System.out.println("丢失后消费");
        for (Order order1 : order) {
            kafkaTemplate.send("1909a",order1.getId(), JSON.toJSONString(order));
        }
    }*/
}
