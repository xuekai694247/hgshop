package com.xuekai.controller;

import com.github.pagehelper.PageInfo;
import com.xuekai.common.AjaxResult;
import com.xuekai.pojo.Carts;
import com.xuekai.pojo.Goods;
import com.xuekai.pojo.User;
import com.xuekai.service.CartsService;
import com.xuekai.service.GoodsService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/good")
public class GoodController {

    @DubboReference
    private GoodsService goodsService;

    @DubboReference
    private CartsService cartsService;

    @Resource
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init(){


    }

    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "8") int pageSize, HttpServletRequest request){
        PageInfo<Goods> goodsPageInfo = goodsService.queryByPage(null, pageNum, pageSize);
        request.setAttribute("pageInfo",goodsPageInfo);
        return "index";
    }

    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult add(Integer id, HttpSession session){
        User user = (User) session.getAttribute("LOGIN_USER");
        if(user==null){
            return new AjaxResult("500","用户未登录","用户未登录");
        }
        int i = goodsService.addCarts(user.getId(), id);
        if(i>0){
            return new AjaxResult("200","添加购物车成功","添加购物车成功");
        }
        return new AjaxResult("500","添加购物车失败","添加购物车失败");

    }
}
