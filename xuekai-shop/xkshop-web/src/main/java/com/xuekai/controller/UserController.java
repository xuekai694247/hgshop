package com.xuekai.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.xuekai.pojo.Carts;
import com.xuekai.pojo.Order;
import com.xuekai.pojo.OrderItem;
import com.xuekai.pojo.User;
import com.xuekai.service.CartsService;
import com.xuekai.service.GoodsService;
import com.xuekai.service.OrderService;
import com.xuekai.service.UserService;
import com.xuekai.utils.AlibabapayGateWayUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @DubboReference
    private UserService userService;

    @DubboReference
    private GoodsService goodsService;

    @DubboReference
    private OrderService orderService;

    @DubboReference
    private CartsService cartsService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private KafkaTemplate kafkaTemplate;

    @PostConstruct
    public void init(){
        System.out.println("监听器设置了");
        kafkaTemplate.setProducerListener(new ProducerListener<String,String>() {
           public void onSuccess(String topic, Integer partition, String key, String value, RecordMetadata recordMetadata) {
               System.out.println("消息发送成功  topic:"+topic+"   key:"+key+"   ,value"+value);
            }
            public void onError(String topic, Integer partition, String key, String value, Exception exception) {
                System.err.println("消息发送失败  topic:"+topic+"   key:"+key+"   ,value"+value);
            }
        });
    }

    @RequestMapping("/toLogin")
    public String toLogin(Model model){
        model.addAttribute("user",new User());
        return "users/user_login";
    }

    @RequestMapping("/login")
    public String login(@Validated@ModelAttribute("user") User user, BindingResult result, HttpServletRequest request){
        if(result.hasErrors()){
            return "users/user_login";
        }
        User login = userService.login(user.getUsername(), user.getPassword(),1);
        if(login!=null){
            request.getSession().setAttribute("LOGIN_USER",login);
            return "redirect:../good/list";
        }
        return "users/user_login";
    }

    @RequestMapping("/home")
    public String home(HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            return "redirect:./toLogin";
        }
        List<Carts> carts=goodsService.listCarts(user.getId());
        request.setAttribute("carts",carts);
        return "users/home";
    }

    @RequestMapping("/createOrder")
    public String createOrder(int ids[],HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            return "redirect:./toLogin";
        }
        Order order=goodsService.createOrder(user.getId(),ids);
        return "redirect:./order?id="+order.getId();
    }

    @RequestMapping("/order")
    public String order(String id,HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            return "redirect:./toLogin";
        }
        Order order=goodsService.getOrder(user.getId(),id);
        request.setAttribute("order",order);
        return "users/order_items";
    }

    @RequestMapping(value = "pay",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String pay(HttpServletRequest request,String id) throws AlipayApiException {
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            return "redirect:./toLogin";
        }
        Order order=goodsService.getOrder(user.getId(),id);
        String payUrl = AlibabapayGateWayUtils.createOrder(id, order.getFactPrice().toString(), "购买商品", "");
        System.out.println(payUrl);
        return payUrl;
    }

    @RequestMapping("/callBack")
    public String callBack(HttpServletRequest request,String out_trade_no,String total_amount){
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            return "redirect:./toLogin";
        }
        Order order = goodsService.getOrder(user.getId(), out_trade_no);

        int i=orderService.updateStatus(order.getId(),1);
        /*redisTemplate.opsForHash().put("order",out_trade_no, JSON.toJSONString(order));*/
        kafkaTemplate.send("1909a","order",JSON.toJSONString(order));
        //清空已经付款的购物车
        //获取商品id
        List<OrderItem> orderItems = order.getOrderItems();
        int a=0;
        for (OrderItem orderItem : orderItems) {
            a+=cartsService.deleteCarts(user.getId(),orderItem.getGoods().getId());
        }
        request.setAttribute("order",order);
        return "index";
    }
}
