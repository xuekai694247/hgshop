package com.xuekai.interceptor;

import com.xuekai.pojo.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        User user = (User) request.getSession().getAttribute("LOGIN_USER");
        if(user==null){
            response.sendRedirect("/user/toLogin");
            return false;
        }
        return true;
    }
}
