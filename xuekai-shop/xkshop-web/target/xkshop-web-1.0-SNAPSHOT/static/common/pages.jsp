<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<c:if test="${pg.total==0}">
				<i>暂无数据</i>
			</c:if>
			<c:if test="${pg.total!=0}">
				<li class="page-item">
					<a class="page-link" href="javascript:goPage(${pg.prePage==0?1:pg.prePage })"aria-label="Previous"> 
					<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
			</c:if>
			<c:forEach items="${pg.navigatepageNums}" var="n">
				<li class="page-item ${n==pg.pageNum?"active":""}"><a
					class="page-link" href="javascript:goPage(${n})">${n}</a></li>
			</c:forEach>
			<c:if test="${pg.total!=0}">
				<li class="page-item"><a class="page-link"
					href="javascript:goPage(${pg.nextPage ==0?pg.pages:pg.nextPage})"
					aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</c:if>
		</ul>
	</nav>

</div>

