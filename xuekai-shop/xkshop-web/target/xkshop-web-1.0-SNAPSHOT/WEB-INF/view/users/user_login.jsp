<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/2/28
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
    <div class="container">
        <form:form action="${pageContext.request.contextPath}/user/login" method="post" modelAttribute="user">
            用户名:<form:input path="username"></form:input>
                    <form:errors path="username"></form:errors><br/>
            密码:<form:input path="password"></form:input>
                    <form:errors path="password"></form:errors><br/>
            <input type="submit" value="登录">
        </form:form>
    </div>
</body>
</html>
