<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/3
  Time: 20:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<form action="${pageContext.request.contextPath}/user/createOrder" method="post">
    <div class="container-fluid">
        <table>
            <tr>
                <td>id</td>
                <td>商品名称</td>
                <td>单价</td>
                <td>购买数量</td>
                <td>操作</td>
            </tr>
            <c:forEach items="${carts}" var="cart" varStatus="status">
                <tr>
                    <td>${status.index+1}<input type="checkbox" name="ids" value="${cart.id}"></td>
                    <td>${cart.goods.name}</td>
                    <td>${cart.goods.price}</td>
                    <td><button>+</button><input type="number" readonly="readonly" value="${cart.buyNum}"></td>
                    <td><input type="button" value="删除" onclick="del(${cart.id})"></td>
                </tr>
            </c:forEach>
            <tr>
                <button class="btn btn-primary" type="submit">添加订单</button>
            </tr>
        </table>
    </div>
</form>
</body>
</html>
