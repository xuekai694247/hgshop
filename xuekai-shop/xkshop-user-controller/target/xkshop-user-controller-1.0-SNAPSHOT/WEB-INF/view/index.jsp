<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/1
  Time: 12:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<div class="container-fluid">
<blockquote class="blockquote text-center">
    <p class="mb-0">昊哥商城</p>
    <footer class="blockquote-footer">From Zhao  <cite title="Source Title">Tian  Hao</cite></footer>
</blockquote>
</div>
<!--导航条-->
<jsp:include page="../navigation.jsp"/>

<div class="container-fluid">
<div class="row">
    <div class="col-3">
        <div class="list-group" id="list-tab" role="tablist">
            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" onclick="enterFun('/brand/list')">品牌管理</a>
            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile" onclick="enterFun('/cat/list')">分类管理</a>
            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages" onclick="enterFun('/good/list')">商品管理</a>
        </div>
    </div>
    <div class="col-9" id="content"></div>
</div>
</div>




<script type="text/javascript">
    $(function(){

    })
    function enterFun(url){
        $("#content").load(url);
    }
</script>
</body>
</html>
