<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/2
  Time: 13:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  添加
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">添加商品</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <table>
                <tr>
                    <td>商品名称</td>
                    <td><input type="text" name="name"></td>
                </tr>
                <tr>
                    <td>商品价格</td>
                    <td><input type="text" name="price"></td>
                </tr>
                <tr>
                    <td>商品描述</td>
                    <td><input type="text" name="title"></td>
                </tr>
                <tr>
                    <td>商品图片</td>
                    <td>
                        <input type="file" id="file" onchange="changeFile()">
                        <input type="hidden" name="picture">
                        <img src="" width="70px" height="70px"/>
                        <input type="hidden" name="fileId">
                    </td>
                </tr>
                <tr>
                    <td>图片描述</td>
                    <td><input type="text" name="viedo"></td>
                </tr>
                <tr>
                    <td>品牌</td>
                    <td>
                        <select name="brandId">
                            <option>--请选择--</option>
                            <c:forEach items="${brands}" var="each">
                                <option value="${each.id}">${each.brandName}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>类别</td>
                    <td>
                        <select name="catId">
                            <option>--请选择--</option>
                            <c:forEach items="${cats}" var="each">
                                <option value="${each.id}">${each.catName}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </table>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="toAdd()">添加</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
