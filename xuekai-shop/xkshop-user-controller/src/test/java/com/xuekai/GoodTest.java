package com.xuekai;

import com.github.pagehelper.PageInfo;
import com.xuekai.es.GoodsEsMapper;
import com.xuekai.pojo.Goods;
import com.xuekai.service.GoodsService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-mvc.xml"})
public class GoodTest {

    @DubboReference
    private GoodsService goodsService;

    @Resource
    private GoodsEsMapper goodsEsMapper;

    @Test
    public void test(){
        goodsEsMapper.deleteAll();
        PageInfo<Goods> goodsPageInfo = goodsService.queryByPage(null, 1, 100);
        goodsPageInfo.getList().forEach(System.out::println);
        goodsEsMapper.saveAll(goodsPageInfo.getList());
    }
}
