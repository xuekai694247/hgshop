package com.xuekai.utils;

import com.github.pagehelper.PageInfo;
import com.xuekai.es.GoodsEsMapper;
import com.xuekai.pojo.File;
import com.xuekai.pojo.Goods;
import com.xuekai.service.FileService;
import com.xuekai.service.GoodsService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@EnableScheduling
@Component
public class TaskSching {

    // 上传的根路径
    @Value("c:/imgs/")
    private String uploadPath;

    // 路径的分隔符号
    private String pathSpit="/";

    @DubboReference
    private FileService fileService;

    @DubboReference
    private GoodsService goodsService;

    @Resource
    private GoodsEsMapper goodsEsMapper;

    @Scheduled(cron = "0 * * * * ?")
    public void test(){
        List<File> files= fileService.list(5);
        for (File file : files) {
            fileService.deleteById(file.getId());
            String filePath=uploadPath+pathSpit+file.getPath();
            java.io.File file1 = new java.io.File(filePath);
            if(file1.exists()){
                file1.delete();
            }
        }
    }

    @Scheduled(cron = "0 * 1 * * ?")
    public void es(){
        //全量同步
        goodsEsMapper.deleteAll();
        PageInfo<Goods> goodsPageInfo = goodsService.queryByPage(null, 1, 100);
        goodsPageInfo.getList().forEach(System.out::println);
        goodsEsMapper.saveAll(goodsPageInfo.getList());
    }
}
