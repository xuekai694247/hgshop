package com.xuekai.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

@Data
@Accessors(chain = true)
@AllArgsConstructor//所有参数的构造器
@NoArgsConstructor//无参构造
public class AjaxResult<T> {
    String code="";
    String errInfo="";
    T data;
}
