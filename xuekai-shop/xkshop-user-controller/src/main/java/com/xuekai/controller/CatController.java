package com.xuekai.controller;

import com.github.pagehelper.PageInfo;
import com.xuekai.pojo.Cats;
import com.xuekai.service.CatsService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/cat")
public class CatController {

    @DubboReference
    private CatsService catsService;

    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "3") int pageSize, HttpServletRequest request){
        PageInfo<Cats> catsPageInfo = catsService.queryByPage(null, pageNum, pageSize);
        request.setAttribute("pageInfo",catsPageInfo);
        return "/cats/cat_list";
    }
}
