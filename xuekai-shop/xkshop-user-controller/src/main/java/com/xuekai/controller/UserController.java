package com.xuekai.controller;

import com.github.pagehelper.PageInfo;
import com.xuekai.pojo.User;
import com.xuekai.service.UserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/user")
public class UserController {
    @DubboReference
    private UserService userService;

    @RequestMapping("/toLogin")
    public String toLogin(Model model){
        model.addAttribute("user",new User());
        return "users/user_login";
    }

    @RequestMapping("/login")
    public String login(@Validated@ModelAttribute("user") User user, BindingResult result, HttpServletRequest request){
        if(result.hasErrors()){
            return "users/user_login";
        }
        User login = userService.login(user.getUsername(), user.getPassword(),1);
        if(login!=null){
            request.getSession().setAttribute("LOGIN_USER",login);
            return "index";
        }
        return "users/user_login";
    }

    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1")int pageNum, @RequestParam(defaultValue = "3")int pageSize, Model model){
        PageInfo<User> userPageInfo = userService.queryByPage(null, pageNum, pageSize);
        model.addAttribute("pageInfo",userPageInfo);
        return "users/user_list";
    }
}
