package com.xuekai.controller;

import com.github.pagehelper.PageInfo;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.xuekai.pojo.Brand;
import com.xuekai.service.BrandService;
import com.xuekai.utils.FileUtils;
import com.xuekai.utils.PDFUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;

@Controller
@RequestMapping("/brand")
public class BrandController {
    @DubboReference
    private BrandService brandService;

    @RequestMapping("/list")
    private String list(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "3") int pageSize, HttpServletRequest request){
        PageInfo<Brand> brandPageInfo = brandService.queryByPage(null, pageNum, pageSize);
        request.setAttribute("pageInfo",brandPageInfo);
        return "brands/list";
    }

    @RequestMapping("/exportPDF")
    public void exportPDF(@RequestParam(defaultValue = "1") int pageNum,@RequestParam(defaultValue = "1") int mode, HttpServletResponse response) throws Exception {
        PageInfo<Brand> brandPageInfo = brandService.queryByPage(null, pageNum, 5);
        //生成文档对象
        Document document = new Document();
        //保存的文档名称
        String fileName= UUID.randomUUID().toString()+".pdf";
        //生成PDF的路径
        PdfWriter pdfWriter = PDFUtils.createDoc(document, "d:/mypdf/"+fileName);
        //添加内容
        ArrayList<String> list = new ArrayList<>();
        list.add("昊哥商城品牌");
        //文档,内容,字体
        PDFUtils.addContent(document,list,18);

        PDFUtils.addTable(pdfWriter,new String[]{"id","名称"},new String[]{"id","brandName"},brandPageInfo.getList(),50,100);
        //关闭
        document.close();
        pdfWriter.close();
        //水印
        PDFUtils.markImage("c:/imgs/2.jpg",new FileInputStream("d:/mypdf/"+fileName),new FileOutputStream("d:/mypdf/mark/"+fileName),"版权所有");
        //下载
        FileUtils.download(response,"d:/mypdf/mark/"+fileName,"昊哥商城品牌.pdf",mode);
    }

    @RequestMapping("/upload")
    public void upload(@RequestParam(defaultValue = "1") int pageNum,@RequestParam(defaultValue = "1") int mode, HttpServletResponse response) throws Exception {
        PageInfo<Brand> brandPageInfo = brandService.queryByPage(null, pageNum, 3);
        Document document = new Document();
        String fileName=UUID.randomUUID().toString()+".pdf";
        //生成的路径
        PdfWriter doc = PDFUtils.createDoc(document, "d:/mypdf/" + fileName);
        ArrayList<String> list = new ArrayList<>();
        list.add("薛凯");
        PDFUtils.addContent(document,list,18);
        PDFUtils.addTable(doc,new String[]{"id","姓名"},new String[]{"id","name"},brandPageInfo.getList(),100,100);
        doc.close();
        document.close();
        PDFUtils.markImage("c:/imgs/2.jpg",new FileInputStream("d:/mypdf/"+fileName),new FileOutputStream("d:/mypdf/mark"+fileName),"赵天昊");
        FileUtils.download(response,"d:/mypdf/mark"+fileName,"阿斯顿.pdf",mode);

    }
}
