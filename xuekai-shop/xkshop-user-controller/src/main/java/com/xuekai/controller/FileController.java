package com.xuekai.controller;

import com.xuekai.common.AjaxResult;
import com.xuekai.pojo.File;
import com.xuekai.service.FileService;
import com.xuekai.utils.FileUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Controller
@RequestMapping("/file")
public class FileController {

    @Resource
    private FileUtils fileUtils;

    @DubboReference
    private FileService fileService;

    @RequestMapping("/upload")
    @ResponseBody
    public AjaxResult upload(MultipartFile file){
        //图片路径
        String filePath = fileUtils.upload(file);
        //判断是否为空
        if(StringUtils.hasText(filePath)){
            File file1 = new File();
            file1.setPath(filePath);
            //调用业务层添加方法
            File insert = fileService.insert(file1);
            //判断是否为空
            if(insert!=null){
                return new AjaxResult("200","",insert);
            }
        }
        return new AjaxResult("500","上传失败","");
    }
}
