package com.xuekai.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.xuekai.common.AjaxResult;
import com.xuekai.es.GoodsEsMapper;
import com.xuekai.pojo.Brand;
import com.xuekai.pojo.Cats;
import com.xuekai.pojo.Goods;
import com.xuekai.pojo.User;
import com.xuekai.service.BrandService;
import com.xuekai.service.CatsService;
import com.xuekai.service.FileService;
import com.xuekai.service.GoodsService;
import com.xuekai.utils.HighLightResultMapper;
import org.apache.dubbo.config.annotation.DubboReference;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/good")
public class GoodController {

    @DubboReference
    private GoodsService goodsService;

    @DubboReference
    private BrandService brandService;

    @DubboReference
    private CatsService catsService;

    @DubboReference
    private FileService fileService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private KafkaTemplate kafkaTemplate;

    @Resource
    private GoodsEsMapper goodsEsMapper;

    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;

    @PostConstruct//预热缓存redis
    public void init(){
        PageInfo<Goods> manger = (PageInfo<Goods>) redisTemplate.opsForValue().get("manger");
        if(manger==null){
            redisTemplate.opsForValue().set("manger",goodsService.queryByPage(null,1,3),10, TimeUnit.MINUTES);
        }
    }

    @RequestMapping("/list")
    public String list(Goods goods,HttpServletRequest request, @RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "3") int pageSize){
        PageInfo<Goods> goodsPageInfo = (PageInfo<Goods>) redisTemplate.opsForValue().get("manger");
        if(goodsPageInfo==null && pageNum==1){
            redisTemplate.opsForValue().set("manger",goodsService.queryByPage(null,1,3),10, TimeUnit.MINUTES);
        }else{
            goodsPageInfo=goodsService.queryByPage(null,pageNum,pageSize);
        }
        List<Brand> brands = brandService.queryAll(null);
        List<Cats> cats = catsService.queryAll(null);
        request.setAttribute("brands",brands);
        request.setAttribute("cats",cats);
        request.setAttribute("pageInfo",goodsPageInfo);
        return "goods/goods_list";
    }

    @RequestMapping("/esList")
    public String esList(Goods goods,HttpServletRequest request, @RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "3") int pageSize){
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if(StringUtils.hasText(goods.getName())){
            queryBuilder.must(QueryBuilders.multiMatchQuery(goods.getName(),"name","title"));
        }
        if(goods.getMinPrice()!=null){
            queryBuilder.must(QueryBuilders.rangeQuery("price").gte(goods.getMinPrice()));
        }
        if(goods.getMaxPrice()!=null){
            queryBuilder.must(QueryBuilders.rangeQuery("price").lte(goods.getMaxPrice()));
        }
        HighlightBuilder.Field name = new HighlightBuilder.Field("name").preTags("<span style='color:red'>").postTags("</span>");
        HighlightBuilder.Field title = new HighlightBuilder.Field("title").preTags("<span style='color:red'>").postTags("</span>");
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(PageRequest.of(pageNum - 1, pageSize))
                .withHighlightFields(name, title)
                .withSort(SortBuilders.fieldSort("id").order(SortOrder.DESC))
                .build();
        AggregatedPage<Goods> goods1 = elasticsearchTemplate.queryForPage(searchQuery, Goods.class, new HighLightResultMapper());
        Page page = new Page(pageNum, pageSize);
        page.setTotal(goods1.getTotalElements());
        PageInfo pageInfo = new PageInfo<>(page);
        pageInfo.setList(goods1.getContent());
        List<Brand> brands = brandService.queryAll(null);
        List<Cats> cats = catsService.queryAll(null);
        request.setAttribute("brands",brands);
        request.setAttribute("cats",cats);
        request.setAttribute("pageInfo",pageInfo);
        request.setAttribute("goods",goods);
        return "goods/goods_list";
    }

    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult add(Goods goods, HttpSession session,Long fileId){
        User user = (User) session.getAttribute("LOGIN_USER");
        goods.setCreator(user.getId());
        Goods insert = goodsService.insert(goods);
        if(insert!=null){
            Goods goods1 = goodsService.queryByIdType(insert.getId());
            //添加es(增量同步)
            goodsEsMapper.save(goods1);
            kafkaTemplate.send("1909a","edit",UUID.randomUUID().toString());
            fileService.deleteById(fileId);
            return new AjaxResult("200","","");
        }
        return new AjaxResult("500","添加失败","");
    }

    @RequestMapping("/del")
    @ResponseBody
    public boolean del(Integer id){
        boolean b = goodsService.deleteById(id);
        if(b){
            //删除(增量同步)
            goodsEsMapper.deleteById(id);
            kafkaTemplate.send("1909a","edit", UUID.randomUUID().toString());
        }
        return b;
    }

    @RequestMapping("/update")
    public String update(Integer id,HttpServletRequest request){
        Goods goods = goodsService.queryById(id);
        List<Brand> brands = brandService.queryAll(null);
        List<Cats> cats = catsService.queryAll(null);
        request.setAttribute("brands",brands);
        request.setAttribute("cats",cats);
        System.out.println(goods);
        request.setAttribute("goods",goods);
        return "goods/update";
    }

    @RequestMapping("/updateGood")
    @ResponseBody
    public AjaxResult updateGood(Goods goods){
        Goods update = goodsService.update(goods);
        if(update!=null){
            //增量同步(修改)
            Goods goods1 = goodsService.queryByIdType(update.getId());
            goodsEsMapper.save(goods1);
            kafkaTemplate.send("1909a","edit",UUID.randomUUID().toString());
            return  new AjaxResult("200","","");
        }
        return  new AjaxResult("500","","");
    }
}
