package com.xuekai.es;

import com.xuekai.pojo.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GoodsEsMapper extends ElasticsearchRepository<Goods,Integer> {
}
