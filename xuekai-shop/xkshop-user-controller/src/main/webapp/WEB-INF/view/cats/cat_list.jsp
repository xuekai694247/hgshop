<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/1
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">类别名称</th>
        <th scope="col">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageInfo.list}" var="each">
        <tr>
            <th scope="row">${each.id}</th>
            <td>${each.catName}</td>
            <td>
                <button type="button" class="btn btn-danger">删除</button>
                <button type="button" class="btn btn-warning">修改</button>
                <button type="button" class="btn btn-success">详情</button>
            </td>

        </tr>
    </c:forEach>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="100">
            <button onclick="exportPDF(0)" class="btn btn-info">PDF导出</button>
            <button onclick="exportPDF(1)" class="btn btn-info">PDF预览</button>
        </td>
    </tr>
    <tr>
        <td colspan="100">
            <button class="btn btn-primary" onclick="goPage(1)">首页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pageNum==1?1:pageInfo.pageNum-1})">上一页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pageNum==pageInfo.pages?pageInfo.pages:pageInfo.pageNum+1})">下一页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pages})">尾页</button>
        </td>
    </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $(function(){

    })
    function goPage(pageNum){
        let url="${pageContext.request.contextPath}/cat/list?pageNum="+pageNum;
        $("#content").load(url);
    }
    function exportPDF(mode){
        window.open("/brand/exportPDF?mode="+mode);
    }
</script>
</body>
</html>
