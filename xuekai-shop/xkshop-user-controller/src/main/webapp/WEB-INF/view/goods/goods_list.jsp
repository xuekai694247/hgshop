<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/1
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<form>
    名称或描述:<input type="text" name="name" value="${goods.name}">
    价格:<input type="text" name="minPrice" value="${goods.minPrice}">~<input type="text" name="maxPrice" value="${goods.maxPrice}">
    <input type="button" value="查询" onclick="searchEs()">
</form>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">商品名称</th>
        <th scope="col">商品价格</th>
        <th scope="col">商品描述</th>
        <th scope="col">商品图片</th>
        <th scope="col">商品品牌</th>
        <th scope="col">商品类别</th>
        <th scope="col"></th>
        <th scope="col">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageInfo.list}" var="each">
        <tr>
            <th scope="row">${each.id}</th>
            <td>${each.name}</td>
            <td>${each.price}</td>
            <td>${each.title}</td>
            <td><img src="http://localhost:90/img/${each.picture}" width="70px" height="70px"></td>
            <td>${each.brandName}</td>
            <td>${each.catName}</td>
            <td>
                <button type="button" class="btn btn-danger" onclick="del(${each.id})">删除</button>
                <button type="button" class="btn btn-warning" onclick="update(${each.id})">修改</button>
                <button type="button" class="btn btn-success">详情</button>
            </td>

        </tr>
    </c:forEach>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="100">
            <button onclick="exportPDF(0)" class="btn btn-info">PDF导出</button>
            <button onclick="exportPDF(1)" class="btn btn-info">PDF预览</button>
        </td>
    </tr>
    <tr>
        <td colspan="100">
            <jsp:include page="model_box.jsp"/>
        </td>
    </tr>
    <tr>
        <td colspan="100">
            <button class="btn btn-primary" onclick="goPage(1)">首页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pageNum==1?1:pageInfo.pageNum-1})">上一页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pageNum==pageInfo.pages?pageInfo.pages:pageInfo.pageNum+1})">下一页</button>
            <button class="btn btn-primary" onclick="goPage(${pageInfo.pages})">尾页</button>
        </td>
    </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $(function(){

    })
    function searchEs(){
        let url="${pageContext.request.contextPath}/good/esList?name="+$("[name='name']").val()+"&minPrice="+$("[name='minPrice']").val()+"&maxPrice="+$("[name='maxPrice']").val();
        $("#content").load(url);
    }
    function goPage(pageNum){
        let url="${pageContext.request.contextPath}/good/list?pageNum="+pageNum;
        $("#content").load(url);
    }
    function changeFile(){
        let formData = new FormData();
        formData.append("file",$("#file")[0].files[0]);
        $.ajax({
            url:"${pageContext.request.contextPath}/file/upload",
            type:"post",
            data:formData,
            dataType:"json",
            processData:false,
            contentType:false,
            success:function(result){
                if(result.code==200){
                    $("[name='picture']").val(result.data.path);
                    $("img").prop("src","http://localhost:90/img/"+result.data.path);
                    $("[name='fileId']").val(result.data.id);
                }
            }
        })
    }
    function toAdd(){
        $.post("${pageContext.request.contextPath}/good/add",$("form").serialize(),function(result){
                if(result.code==200){
                    alert("添加成功");
                    $("#content").load("${pageContext.request.contextPath}/good/list");
                }
        },"json")

    }
    function exportPDF(mode){
        window.open("/brand/exportPDF?mode="+mode);
    }

    function del(id){
        if(confirm("您确定要删除吗？")){
            $.post("${pageContext.request.contextPath}/good/del",{id:id},function(res){
                if(res){
                    alert("删除成功");
                    $("#content").load("${pageContext.request.contextPath}/good/list");
                }
            },"json")
        }
    }

    function update(id){
        $("#content").load("${pageContext.request.contextPath}/good/update?id="+id);
    }

</script>
</body>
</html>
