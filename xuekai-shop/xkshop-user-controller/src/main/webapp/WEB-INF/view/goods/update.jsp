<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuekai
  Date: 2022/3/2
  Time: 13:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>$</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap4/css/bootstrap.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap-treeview/js/bootstrap-treeview.js"></script>
    <script src="${pageContext.request.contextPath}/static/bootstrap4/js/bootstrap.js"></script>
</head>
<body>
<form>
    <input type="hidden" name="id" value="${goods.id}">
            <div class="modal-body">
                    <table>
                        <tr>
                            <td>商品名称</td>
                            <td><input type="text" name="name" value="${goods.name}"></td>
                        </tr>
                        <tr>
                            <td>商品价格</td>
                            <td><input type="text" name="price" value="${goods.price}"></td>
                        </tr>
                        <tr>
                            <td>在售状态</td>
                            <td>
                                <input type="radio" name="isSelling" value="0" ${goods.isSelling==0?'checked':''}>未售
                                <input type="radio" name="isSelling" value="1" ${goods.isSelling==1?'checked':''}>在售
                            </td>
                        </tr>
                        <tr>
                            <td>商品描述</td>
                            <td><input type="text" name="title" value="${goods.title}"></td>
                        </tr>
                        <tr>
                            <td>商品图片</td>
                            <td>
                                <input type="file" id="file" onchange="changeFile()">
                                <input type="hidden" name="picture" value="${goods.picture}">
                                <img src="http://127.0.0.1:90/img/${goods.picture}" width="70px" height="70px"/>
                                <input type="hidden" name="fileId">
                            </td>
                        </tr>
                        <tr>
                            <td>图片描述</td>
                            <td><input type="text" name="viedo" value="${goods.viedo}"></td>
                        </tr>
                        <tr>
                            <td>品牌</td>
                            <td>
                                <select name="brandId">
                                    <option>--请选择--</option>
                                    <c:forEach items="${brands}" var="each">
                                        <option value="${each.id}">${each.brandName}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>类别</td>
                            <td>
                                <select name="catId">
                                    <option>--请选择--</option>
                                    <c:forEach items="${cats}" var="each">
                                        <option value="${each.id}">${each.catName}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateGoods()">修改</button>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    $(function(){
        $("[name='brandId']").val(${goods.brandId});
        $("[name='catId']").val(${goods.catId});
    })
    function updateGoods(){
        $.post("${pageContext.request.contextPath}/good/updateGood",$("form").serialize(),function(result){
            if(result.code==200){
                $("#content").load("${pageContext.request.contextPath}/good/list");
            }else {
                alert("修改失败");
            }
        },"json")
    }
</script>
</body>
</html>
