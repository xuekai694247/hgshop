package com.xuekai.service;

import com.xuekai.pojo.OrderItem;
import com.github.pagehelper.PageInfo;

/**
 * (OrderItem)表服务接口
 *
 * @author xk
 * @since 2022-03-03 19:29:30
 */
public interface OrderItemService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    OrderItem queryById(Long id);

    /**
     * 分页查询
     *
     * @param orderItem 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<OrderItem> queryByPage(OrderItem orderItem,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    int insert(OrderItem orderItem);

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    int update(OrderItem orderItem);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
