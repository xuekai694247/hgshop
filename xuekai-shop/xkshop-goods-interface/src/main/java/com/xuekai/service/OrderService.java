package com.xuekai.service;

import com.xuekai.pojo.Order;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (Order)表服务接口
 *
 * @author xk
 * @since 2022-03-03 19:29:14
 */
public interface OrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Order queryById(String id);

    /**
     * 分页查询
     *
     * @param order 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<Order> queryByPage(Order order,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int insert(Order order);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int update(Order order);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    /**
     * 修改状态
     * @param id
     * @return
     */
    int updateStatus(String id,Integer status);

    /**
     * 查询5分钟之前
     * @param i
     * @return
     */
    List<Order> listUnpay(int i);

    /**
     * 添加资金明细
     * @param factPrice
     * @param orderId
     * @return
     */
    int insertFlow(Double factPrice, String orderId);
}
