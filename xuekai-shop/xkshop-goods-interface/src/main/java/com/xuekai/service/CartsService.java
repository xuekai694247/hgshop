package com.xuekai.service;

import com.xuekai.pojo.Carts;
import com.github.pagehelper.PageInfo;

/**
 * (Carts)表服务接口
 *
 * @author xk
 * @since 2022-03-03 17:25:45
 */
public interface CartsService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Carts queryById(Integer id);

    /**
     * 分页查询
     *
     * @param carts 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<Carts> queryByPage(Carts carts,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    int insert(Carts carts);

    /**
     * 修改数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    int update(Carts carts);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 清除已经购买的商品
     * @param userId
     * @param goodId
     * @return
     */
    int deleteCarts(Integer userId, Integer goodId);
}
