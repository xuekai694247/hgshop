package com.xuekai.service;

import com.xuekai.pojo.Cats;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (Cats)表服务接口
 *
 * @author xk
 * @since 2022-03-02 13:39:14
 */
public interface CatsService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Cats queryById(Integer id);

    /**
     * 分页查询
     *
     * @param cats 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<Cats> queryByPage(Cats cats,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    int insert(Cats cats);

    /**
     * 修改数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    int update(Cats cats);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    /**
     * 查询指定行数据
     *
     * @param cats 查询条件
     * @return 对象列表
     */
    List<Cats> queryAll(Cats cats);

}
