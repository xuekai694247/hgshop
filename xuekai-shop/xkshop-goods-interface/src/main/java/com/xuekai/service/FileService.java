package com.xuekai.service;

import com.xuekai.pojo.File;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (File)表服务接口
 *
 * @author xk
 * @since 2022-03-02 10:05:56
 */
public interface FileService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    File queryById(Long id);

    /**
     * 分页查询
     *
     * @param file 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<File> queryByPage(File file,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    File insert(File file);

    /**
     * 修改数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    int update(File file);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

    /**
     * 查询5分钟之前的数据
     * @param i
     * @return
     */
    List<File> list(int i);
}
