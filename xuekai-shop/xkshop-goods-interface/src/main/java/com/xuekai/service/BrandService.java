package com.xuekai.service;

import com.xuekai.pojo.Brand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (Brand)表服务接口
 *
 * @author xk
 * @since 2022-03-01 12:26:25
 */
public interface BrandService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Brand queryById(Integer id);

    /**
     * 分页查询
     *
     * @param brand 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<Brand> queryByPage(Brand brand,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param brand 实例对象
     * @return 影响行数
     */
    int insert(Brand brand);

    /**
     * 修改数据
     *
     * @param brand 实例对象
     * @return 影响行数
     */
    int update(Brand brand);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param brand 查询条件
     * @return 对象列表
     */
    List<Brand> queryAll(Brand brand);

}
