package com.xuekai.service;

import com.xuekai.pojo.Carts;
import com.xuekai.pojo.Goods;
import com.github.pagehelper.PageInfo;
import com.xuekai.pojo.Order;

import java.util.List;

/**
 * (Goods)表服务接口
 *
 * @author xk
 * @since 2022-03-02 10:00:02
 */
public interface GoodsService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Goods queryById(Integer id);

    /**
     * 分页查询
     *
     * @param goods 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<Goods> queryByPage(Goods goods,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    Goods insert(Goods goods);

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    Goods update(Goods goods);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 修改购物车
     * @param carts
     * @return
     */
    Carts updateCarts(Carts carts);


    /**
     * 添加购物车
     * @param id
     * @param id1
     * @return
     */
    int addCarts(Integer id, Integer id1);

    /*    *//**
     * 查询所有购物车
     * @param userId
     * @return
     */
    List<Carts>  listCarts(Integer userId);

    Order createOrder(Integer userId, int[] ids);


    Order getOrder(Integer userId, String orderId);

    List<Goods> queryAll();

    /**
     * 查询对象
     * @param id
     * @return
     */
    Goods queryByIdType(Integer id);
}
