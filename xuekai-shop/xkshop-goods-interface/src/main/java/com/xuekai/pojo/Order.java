package com.xuekai.pojo;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2022-03-03 19:29:14
 */
@Data
public class Order implements Serializable {
    private static final long serialVersionUID = -64497410964929631L;
    /**
     * 订单编号
     */
    private String id;

    private List<OrderItem> orderItems;
    /**
     * 下单人
     */
    private Integer userId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 0 待付 1已经支付 2 已经取消
     */
    private Integer status;
    /**
     * 应付
     */
    private Double price;
    /**
     * 实付
     */
    private Double factPrice;


}

