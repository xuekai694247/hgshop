package com.xuekai.pojo;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * (Goods)实体类
 *
 * @author makejava
 * @since 2022-03-02 10:00:02
 */
@Data
@Document(indexName = "goods_name",type = "goods_type")
public class Goods implements Serializable {
    private static final long serialVersionUID = -73125887585262128L;
    /**
     * 主键
     */
    @Id
    private Integer id;
    /**
     * 商品名称
     */
    private String name;
    /**
     * 商品描述
     */
    private String title;
    /**
     * 是否在售
     */
    private Integer isSelling;
    /**
     * 图片
     */
    private String picture;
    
    private String viedo;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private Integer creator;
    /**
     * 品牌id
     */
    private Integer brandId;
    private String brandName;
    /**
     * 分类id
     */
    private Integer catId;
    private String catName;
    private Double price;

    //扩展
    private Double minPrice;
    private Double maxPrice;

}

