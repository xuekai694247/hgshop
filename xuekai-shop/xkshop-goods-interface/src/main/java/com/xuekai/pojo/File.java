package com.xuekai.pojo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * (File)实体类
 *
 * @author makejava
 * @since 2022-03-02 10:05:54
 */
@Data
public class File implements Serializable {
    private static final long serialVersionUID = 476915246262379862L;
    
    private Long id;
    /**
     * 类型
     */
    private String type;
    /**
     * 图片地址
     */
    private String path;
    
    private Integer used;
    private Date createTime;


}

