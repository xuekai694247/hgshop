package com.xuekai.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * (OrderItem)实体类
 *
 * @author makejava
 * @since 2022-03-03 19:29:30
 */
@Data
public class OrderItem implements Serializable {
    private static final long serialVersionUID = -91279002699544618L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 订单id
     */
    private String orderId;
    /**
     * 商品id
     */
    private Integer goodsId;
    private Goods goods;
    /**
     * 商品价格
     */
    private Double price;
    /**
     * 购买数量
     */
    private Integer buyNum;


}

