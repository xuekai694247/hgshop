package com.xuekai.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * (Brand)实体类
 *
 * @author makejava
 * @since 2022-03-01 12:26:24
 */
@Data
public class Brand implements Serializable {
    private static final long serialVersionUID = 142797886827539646L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 品牌名称
     */
    private String brandName;
    /**
     * 逻辑删除
     */
    private Integer delFlag;


}

