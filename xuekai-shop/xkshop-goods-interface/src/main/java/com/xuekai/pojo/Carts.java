package com.xuekai.pojo;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;

/**
 * (Carts)实体类
 *
 * @author makejava
 * @since 2022-03-03 17:25:45
 */
@Data
public class Carts implements Serializable {
    private static final long serialVersionUID = -71249842638728368L;
    /**
     * 购物车
     */
    private Integer id;
    /**
     * 下单人
     */
    private Integer userId;
    /**
     * 商品编号
     */
    private Integer goodsId;
    private Goods goods;
    /**
     * 数量
     */
    private Integer buyNum;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;


}

