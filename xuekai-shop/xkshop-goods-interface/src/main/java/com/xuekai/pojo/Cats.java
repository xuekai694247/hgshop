package com.xuekai.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * (Cats)实体类
 *
 * @author makejava
 * @since 2022-03-02 13:39:14
 */
@Data
public class Cats implements Serializable {
    private static final long serialVersionUID = 946693444482670688L;
    /**
     * 分类主键
     */
    private Integer id;
    /**
     * 分类名称
     */
    private String catName;
    /**
     * 逻辑删除 0正常 1:删除
     */
    private Integer status;


}

