package com.xuekai.service.impl;



import com.xuekai.pojo.Carts;
import com.xuekai.pojo.User;
import com.xuekai.mapper.UserMapper;
import com.xuekai.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author xk
 * @since 2022-03-01 11:12:28
 */
@DubboService
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;



    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Integer id) {
        return this.userMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param user 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<User> queryByPage(User user,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.userMapper.queryAll(user));
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(User user) {
        return this.userMapper.insert(user);
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    @Override
    public int update(User user) {
        return this.userMapper.update(user);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.userMapper.deleteById(id) > 0;
    }

    @Override
    public User login(String username, String password,int role) {
        User login = this.userMapper.login(username,role);
        if(login==null){
            return null;
        }
        String s = DigestUtils.md5Hex(password);
        System.out.println(s);
        if(!login.getPassword().equals(s)){
            return null;
        }
        return login;
    }

}
