package com.xuekai.pojo;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2022-03-01 11:12:19
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 402560113224922305L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 真实名称
     */
    private String realName;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户权限
     */
    private Integer role;
    /**
     * 创建时间
     */
    private Date createTime;


}

