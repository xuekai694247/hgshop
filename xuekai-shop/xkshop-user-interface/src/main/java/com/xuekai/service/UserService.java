package com.xuekai.service;

import com.xuekai.pojo.Carts;
import com.xuekai.pojo.User;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author xk
 * @since 2022-03-01 11:12:19
 */
public interface UserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 分页查询
     *
     * @param user 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    PageInfo<User> queryByPage(User user,int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int update(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    User login(String username,String password,int role);


}
