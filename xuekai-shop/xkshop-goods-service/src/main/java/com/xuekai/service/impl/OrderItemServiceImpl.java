package com.xuekai.service.impl;

import com.xuekai.pojo.OrderItem;
import com.xuekai.mapper.OrderItemMapper;
import com.xuekai.service.OrderItemService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;

/**
 * (OrderItem)表服务实现类
 *
 * @author xk
 * @since 2022-03-03 19:29:37
 */
@DubboService
public class OrderItemServiceImpl implements OrderItemService {
    @Resource
    private OrderItemMapper orderItemMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public OrderItem queryById(Long id) {
        return this.orderItemMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param orderItem 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<OrderItem> queryByPage(OrderItem orderItem,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.orderItemMapper.queryAll(orderItem));
    }

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(OrderItem orderItem) {
        return this.orderItemMapper.insert(orderItem);
    }

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    @Override
    public int update(OrderItem orderItem) {
        return this.orderItemMapper.update(orderItem);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.orderItemMapper.deleteById(id) > 0;
    }
}
