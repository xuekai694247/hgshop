package com.xuekai.service.impl;

import com.xuekai.pojo.Order;
import com.xuekai.mapper.OrderMapper;
import com.xuekai.service.OrderService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Order)表服务实现类
 *
 * @author xk
 * @since 2022-03-03 19:29:23
 */
@DubboService
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Order queryById(String id) {
        return this.orderMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param order 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<Order> queryByPage(Order order,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.orderMapper.queryAll(order));
    }

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(Order order) {
        return this.orderMapper.insert(order);
    }

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Order order) {
        return this.orderMapper.update(order);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.orderMapper.deleteById(id) > 0;
    }

    @Override
    public int updateStatus(String id,Integer status) {
        return this.orderMapper.updateStatus(id,status);
    }

    @Override
    public List<Order> listUnpay(int i) {
        return this.orderMapper.listUnpay(i);
    }

    @Override
    public int insertFlow(Double factPrice, String orderId) {
        return this.orderMapper.insertFlow(factPrice,orderId);
    }


}
