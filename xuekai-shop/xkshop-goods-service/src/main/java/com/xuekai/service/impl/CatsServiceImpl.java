package com.xuekai.service.impl;

import com.xuekai.pojo.Cats;
import com.xuekai.mapper.CatsMapper;
import com.xuekai.service.CatsService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Cats)表服务实现类
 *
 * @author xk
 * @since 2022-03-02 13:39:21
 */
@DubboService
public class CatsServiceImpl implements CatsService {
    @Resource
    private CatsMapper catsMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Cats queryById(Integer id) {
        return this.catsMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param cats 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<Cats> queryByPage(Cats cats,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.catsMapper.queryAll(cats));
    }

    /**
     * 新增数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(Cats cats) {
        return this.catsMapper.insert(cats);
    }

    /**
     * 修改数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Cats cats) {
        return this.catsMapper.update(cats);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.catsMapper.deleteById(id) > 0;
    }

    @Override
    public List<Cats> queryAll(Cats cats) {
        return this.catsMapper.queryAll(cats);
    }
}
