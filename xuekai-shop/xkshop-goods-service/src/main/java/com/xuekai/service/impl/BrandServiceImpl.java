package com.xuekai.service.impl;

import com.xuekai.pojo.Brand;
import com.xuekai.mapper.BrandMapper;
import com.xuekai.service.BrandService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Brand)表服务实现类
 *
 * @author xk
 * @since 2022-03-01 12:26:37
 */
@DubboService
public class BrandServiceImpl implements BrandService {
    @Resource
    private BrandMapper brandMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Brand queryById(Integer id) {
        return this.brandMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param brand 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<Brand> queryByPage(Brand brand,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.brandMapper.queryAll(brand));
    }

    /**
     * 新增数据
     *
     * @param brand 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(Brand brand) {
        return this.brandMapper.insert(brand);
    }

    /**
     * 修改数据
     *
     * @param brand 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Brand brand) {
        return this.brandMapper.update(brand);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.brandMapper.deleteById(id) > 0;
    }

    @Override
    public List<Brand> queryAll(Brand brand) {
        return this.brandMapper.queryAll(brand);
    }
}
