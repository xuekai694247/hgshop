package com.xuekai.service.impl;

import com.xuekai.pojo.Carts;
import com.xuekai.mapper.CartsMapper;
import com.xuekai.service.CartsService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;

/**
 * (Carts)表服务实现类
 *
 * @author xk
 * @since 2022-03-03 17:25:52
 */
@DubboService
public class CartsServiceImpl implements CartsService {
    @Resource
    private CartsMapper cartsMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Carts queryById(Integer id) {
        return this.cartsMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param carts 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<Carts> queryByPage(Carts carts,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.cartsMapper.queryAll(carts));
    }

    /**
     * 新增数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(Carts carts) {
        return this.cartsMapper.insert(carts);
    }

    /**
     * 修改数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Carts carts) {
        return this.cartsMapper.update(carts);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.cartsMapper.deleteById(id) > 0;
    }

    @Override
    public int deleteCarts(Integer userId, Integer goodId) {
        return this.cartsMapper.deleteCarts(userId,goodId);
    }
}
