package com.xuekai.service.impl;

import com.xuekai.pojo.File;
import com.xuekai.mapper.FileMapper;
import com.xuekai.service.FileService;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;

/**
 * (File)表服务实现类
 *
 * @author xk
 * @since 2022-03-02 10:10:54
 */
@DubboService
public class FileServiceImpl implements FileService {
    @Resource
    private FileMapper fileMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public File queryById(Long id) {
        return this.fileMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param file 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<File> queryByPage(File file,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.fileMapper.queryAll(file));
    }

    /**
     * 新增数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    @Override
    public File insert(File file) {
        int insert = this.fileMapper.insert(file);
        if(insert>0){
            return file;
        }
        return null;
    }

    /**
     * 修改数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    @Override
    public int update(File file) {
        return this.fileMapper.update(file);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.fileMapper.deleteById(id) > 0;
    }

    @Override
    public List<File> list(int i) {
        return this.fileMapper.list(i);
    }
}
