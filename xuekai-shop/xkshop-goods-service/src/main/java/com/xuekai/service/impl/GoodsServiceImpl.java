package com.xuekai.service.impl;

import com.xuekai.mapper.CartsMapper;
import com.xuekai.pojo.Carts;
import com.xuekai.pojo.Goods;
import com.xuekai.mapper.GoodsMapper;
import com.xuekai.pojo.Order;
import com.xuekai.pojo.OrderItem;
import com.xuekai.service.GoodsService;
import com.xuekai.service.OrderItemService;
import com.xuekai.service.OrderService;
import com.xuekai.utils.SnowflakeConfig;
import org.apache.dubbo.config.annotation.DubboService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * (Goods)表服务实现类
 *
 * @author xk
 * @since 2022-03-02 10:05:07
 */
@DubboService
public class GoodsServiceImpl implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private CartsMapper cartsMapper;

    @Resource
    private OrderService orderService;

    @Resource
    private OrderItemService orderItemService;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Goods queryById(Integer id) {
        return this.goodsMapper.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param goods 筛选条件
     * @param pageNum 起始页数
     * @param pageSize 分页偏移量
     * @return 查询结果
     */
    @Override
    public PageInfo<Goods> queryByPage(Goods goods,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(this.goodsMapper.queryAll(goods));
    }

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    @Override
    public Goods insert(Goods goods) {
        int insert = this.goodsMapper.insert(goods);
        if(insert>0){
            return goods;
        }
        return null;
    }

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    @Override
    public Goods update(Goods goods) {
        int update = this.goodsMapper.update(goods);
        if(update>0){
            return goods;
        }
        return null;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.goodsMapper.deleteById(id) > 0;
    }



    @Override
    public Carts updateCarts(Carts carts) {
        int update=this.goodsMapper.updateCarts(carts);
        if(update>0){
            return carts;
        }
        return null;
    }

    @Override
    public int addCarts(Integer userId, Integer goodId) {
        Carts carts = goodsMapper.selectIdGoodId(userId, goodId);
        int i=0;
        if(carts!=null){
            i=goodsMapper.updateCarts(carts);
        }else{
            Carts carts1 = new Carts();
            carts1.setUserId(userId);
            carts1.setGoodsId(goodId);
            i=cartsMapper.insert(carts1);
        }
        return i;
    }

    @Override
    public List<Carts> listCarts(Integer userId) {
        return this.cartsMapper.listUser(userId);
    }

    @Override
    public Order createOrder(Integer userId, int[] ids) {
        List<Carts> carts=cartsMapper.listIds(userId,ids);
        //主键id  雪花算法
        String idStr = SnowflakeConfig.snowflakeIdStr();

        //订单表
        Order order = new Order();
        order.setUserId(userId);
        order.setId(idStr);
        Double price=new Double(0);
        ArrayList<OrderItem> list = new ArrayList<>();
        //订单明细
        for (Carts cart : carts) {
            OrderItem orderItem = new OrderItem();
            orderItem.setGoodsId(cart.getGoodsId());
            orderItem.setBuyNum(cart.getBuyNum());
            orderItem.setPrice(cart.getGoods().getPrice());
            orderItem.setOrderId(idStr);
            price+=cart.getGoods().getPrice()*cart.getBuyNum();
            list.add(orderItem);
        }

        order.setPrice(price);
        order.setFactPrice(price);
        //添加订单表
        orderService.insert(order);
        //循环插入订单明细表
        for (OrderItem orderItem : list) {
            orderItemService.insert(orderItem);
        }
        return order;
    }

    @Override
    public Order getOrder(Integer userId, String orderId) {
        //根据用户id和订单id查询出相应的订单
        Order order=goodsMapper.selectIdOrderId(userId,orderId);
        System.out.println(order);
        //根据订单id查询出订单明细表
        List<OrderItem> orderItems=goodsMapper.selectOrderId(order.getId());
        System.out.println(orderItems);
        order.setOrderItems(orderItems);
        return order;
    }

    @Override
    public List<Goods> queryAll() {
        return this.queryAll();
    }

    @Override
    public Goods queryByIdType(Integer id) {
        return this.goodsMapper.queryByIdType(id);
    }


}
