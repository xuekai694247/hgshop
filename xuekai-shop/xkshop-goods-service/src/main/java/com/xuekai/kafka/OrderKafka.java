package com.xuekai.kafka;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.xuekai.pojo.Goods;
import com.xuekai.pojo.Order;
import com.xuekai.service.GoodsService;
import com.xuekai.service.OrderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;

import javax.annotation.Resource;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class OrderKafka implements AcknowledgingMessageListener<String,String> {

    @Resource
    private RedisTemplate redisTemplate;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    @Override
    public void onMessage(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        if(record.key().equals("order")){
            Order order = JSON.parseObject(record.value(), Order.class);
            //防止重复消费
            if(!redisTemplate.opsForValue().setIfAbsent(order.getId(),"",10,TimeUnit.MINUTES)){
                return;
            }
            int i = orderService.insertFlow(order.getFactPrice(), order.getId());
        }else if(record.key().equals("edit")){
            if(!redisTemplate.opsForValue().setIfAbsent(record.value(),"",10,TimeUnit.MINUTES)){
                System.out.println("编辑重复消费");
                return;
            }
            PageInfo<Goods> goodsPageInfo = goodsService.queryByPage(null, 1, 3);
            redisTemplate.opsForValue().set("manger",goodsPageInfo,10, TimeUnit.MINUTES);
            System.out.println("redis   数据更新成功");
        }

        acknowledgment.acknowledge();//手动提交
    }
}
