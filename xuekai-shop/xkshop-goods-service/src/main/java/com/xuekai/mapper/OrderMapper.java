package com.xuekai.mapper;

import com.xuekai.pojo.Order;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Order)表数据库访问层
 *
 * @author xk
 * @since 2022-03-03 19:29:23
 */
@Repository
public interface OrderMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Order queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param order 查询条件
     * @return 对象列表
     */
    List<Order> queryAll(Order order);

    /**
     * 统计总行数
     *
     * @param order 查询条件
     * @return 总行数
     */
    long count(Order order);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int insert(Order order);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Order> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Order> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Order> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Order> entities);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int update(Order order);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    /**
     * 修改状态
     * @param id
     * @return
     */
    int updateStatus(@Param("id") String id,@Param("status") Integer status);

    /**
     * 查询5分钟之前的数据
     * @param i
     * @return
     */
    List<Order> listUnpay(int i);

    /**
     * 添加资金流水明细
     * @param factPrice
     * @param orderId
     * @return
     */
    int insertFlow(@Param("price") Double factPrice, @Param("orderId") String orderId);
}
