package com.xuekai.mapper;

import com.xuekai.pojo.Carts;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Carts)表数据库访问层
 *
 * @author xk
 * @since 2022-03-03 17:25:52
 */
@Repository
public interface CartsMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Carts queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param carts 查询条件
     * @return 对象列表
     */
    List<Carts> queryAll(Carts carts);

    /**
     * 统计总行数
     *
     * @param carts 查询条件
     * @return 总行数
     */
    long count(Carts carts);

    /**
     * 新增数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    int insert(Carts carts);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Carts> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Carts> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Carts> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Carts> entities);

    /**
     * 修改数据
     *
     * @param carts 实例对象
     * @return 影响行数
     */
    int update(Carts carts);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 查询用户购物车数据
     * @param userId
     * @return
     */
    List<Carts> listUser(Integer userId);

    /**
     * 列出购物车数据
     * @param userId
     * @param ids
     * @return
     */
    List<Carts> listIds(@Param("userId") Integer userId, @Param("ids") int[] ids);

    /**
     * 清除购物车
     * @param userId
     * @param goodId
     * @return
     */
    int deleteCarts(@Param("userId") Integer userId, @Param("goodId") Integer goodId);
}
