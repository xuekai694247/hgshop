package com.xuekai.mapper;

import com.xuekai.pojo.Carts;
import com.xuekai.pojo.Goods;
import com.xuekai.pojo.Order;
import com.xuekai.pojo.OrderItem;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Goods)表数据库访问层
 *
 * @author xk
 * @since 2022-03-02 10:05:07
 */
@Repository
public interface GoodsMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Goods queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param goods 查询条件
     * @return 对象列表
     */
    List<Goods> queryAll(Goods goods);

    /**
     * 统计总行数
     *
     * @param goods 查询条件
     * @return 总行数
     */
    long count(Goods goods);

    /**
     * 新增数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    int insert(Goods goods);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Goods> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Goods> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Goods> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Goods> entities);

    /**
     * 修改数据
     *
     * @param goods 实例对象
     * @return 影响行数
     */
    int update(Goods goods);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 查询是否存在
     * @param userId
     * @param goodId
     * @return
     */
    Carts selectIdGoodId(@Param("userId") Integer userId, @Param("goodId") Integer goodId);

    /**
     * 修改购物车
     * @param carts
     * @return
     */
    int updateCarts(Carts carts);


    /**
     * 查询订单
     * @param userId
     * @param orderId
     * @return
     */
    Order selectIdOrderId(@Param("userId") Integer userId, @Param("orderId") String orderId);

    /**
     * 查询出订单明细数据
     * @param orderId
     * @return
     */
    List<OrderItem> selectOrderId(String orderId);

    /**
     * 查询
     * @param id
     * @return
     */
    Goods queryByIdType(Integer id);
}
