package com.xuekai.mapper;

import com.xuekai.pojo.Cats;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Cats)表数据库访问层
 *
 * @author xk
 * @since 2022-03-02 13:39:21
 */
@Repository
public interface CatsMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Cats queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param cats 查询条件
     * @return 对象列表
     */
    List<Cats> queryAll(Cats cats);

    /**
     * 统计总行数
     *
     * @param cats 查询条件
     * @return 总行数
     */
    long count(Cats cats);

    /**
     * 新增数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    int insert(Cats cats);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Cats> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Cats> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Cats> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Cats> entities);

    /**
     * 修改数据
     *
     * @param cats 实例对象
     * @return 影响行数
     */
    int update(Cats cats);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}
